# parser qrc file's 


import sys
def parse_qrc_file(qrc_file):
    with open(qrc_file, 'r') as f:
        lines = f.readlines()
    for line in lines:
        line = line.strip()
        if line.startswith('<file>'):
            file_name = line[6:-7]
            print(file_name)

def main(argv):
    parse_qrc_file(argv[1])

if __name__ == "__main__":
    try:
        main(sys.argv)
    except RuntimeError as e:
        sys.exit(1)
# auto detect  file that need uic/moc/rcc from sources

import sys, json, re
from enum import Enum


def IsInclude(word, text) -> bool:
    pattern = r"\b" + word + r"\b"

    if re.search(pattern, text):
        return True
    else:
        return False


class FileType(Enum):
    uic = 1
    moc = 2
    qcc = 3
    src = 4


def decide(filename):
    if filename.endswith(".ui"):
        return FileType.uic
    elif filename.endswith(".qrc"):
        return FileType.qcc
    elif filename.endswith(".h"):
        f = open(filename, "r", errors= "ignore")
        for i in f.readlines():
            if IsInclude("Q_OBJECT", i):
                return FileType.moc
        return FileType.src
    else:
        return FileType.src


def main(argv):
    f = open(argv[1], "r")
    lst = f.readlines()
    assert len(lst) % 2 == 0
    data = {}
    src = []
    moc = []
    ui = []
    qrc = []
    for i in range(len(lst) // 2):
        t = decide(lst[i + len(lst) // 2].strip())
        if t == FileType.src:
            src.append(lst[i].strip())
        elif t == FileType.moc:
            moc.append(lst[i].strip())
        elif t == FileType.uic:
            ui.append(lst[i].strip())
        elif t == FileType.qcc:
            qrc.append(lst[i].strip())
    if src:
        data["src"] = src
    if moc:
        data["moc"] = moc
    if ui:
        data["ui"] = ui
    if qrc:
        data["qrc"] = qrc
    print(json.dumps(data, indent=4))


if __name__ == "__main__":
    try:
        main(sys.argv)
    except RuntimeError as e:
        sys.exit(1)
